from setuptools import setup, find_packages
import savopy

setup(name='savopy',
      version=savopy.__version__,
      description='Analysis of sensory and consumer data',
      install_requires=[
          'numpy>=1.13.3',
          'scipy>=0.19.1'
          ],
      url='git@gitlab.com:edgarrmondragon/savopy',
      author='Edgar Ramírez Mondragón',
      author_email='edgarrm358@gmail.com',
      license='MIT',
      packages=find_packages(),
      zip_safe=False)
