# -*- coding: utf-8 -*-
# Page's test for ordered alternatives

import numpy as np
from scipy.stats import rankdata, norm
from collections import namedtuple

PageResult = namedtuple('PageResult',
                        ['alternative', 'ranksum', 'L', 'Z', 'pvalue'])


def page_test(raw_data, rankings='asc', continuity_correction=True):
    '''Page's Test: Hypothesis testing for ordered alternatives.

    In statistics, the Page test for multiple comparisons between ordered
    correlated variables is the counterpart of Spearman's rank correlation
    coefficient which summarizes the association of continuous variables.
    It is also known as Page's trend test or Page's L test. It is a repeated
    measure test.

    The Page test is useful where:

        - there are three or more conditions,
        - a number of subjects (or other randomly sampled entities) are all
          observed in each of them, and
        - we predict that the observations will have a particular order.

    The test considers the null hypothesis that, for n conditions, where mi is
    a measure of the central tendency of the ith condition,

        m1 = m2 = m3 = ... = mn

    against the alternative hypothesis that

        m1 <= m2 <= m3 <= ... <= mn,

    with at least one strict inequality.

    It has more statistical power than the Friedman test against the
    alternative that there is a difference in trend. Friedman's test considers
    the alternative hypothesis that the central tendencies of the observations
    under the n conditions are different without specifying their order.

    https://kevinstadler.github.io/cultevo/articles/page.test.html
    https://en.wikipedia.org/wiki/Page's_trend_test

    Args:
        raw_data (array-like): b * v array with data, where b is the number of
            judges and v is the number of product.
        rankings ({‘ascending’, ‘descending’} or array of ints): a one
            dimensional array with the hypothetical rankings of the products.
        continiuty_correction (bool): if true, applies continuity correction
            to the calculated z-value.

    Returns:
        PageResult (namedtuple):
            alternative (string): a string containing the alternative
                hypothesis according to the presumed rankings.
            ranksum (np.array): a one-dimensional numpy array of size v with
                the sum of ranks for every product.
            L (float): weighted sum of the column (product) ranking totals.
            Z (float): z-value used for testing.

    Raises:
        ValueError: If value of 'rankings' is not in {'asc', 'desc'} or if the
            length of the array is not compatible with 'raw_data'.
        ValueError: If the design is not balanced, meaning products are not
            ranked the same number of times and/or judges don't rank the same
            number of products.

    Example:

        >>> data = [[3, 4, 2, 1],
        ...         [4, 2, 1, 3],
        ...         [4, 2, 3, 1],
        ...         [4, 1, 3, 2],
        ...         [2, 4, 3, 1],
        ...         [4, 3, 1, 2]]

        >>> page = page_test(data, rankings='desc',
        ...                  continuity_correction=False)
        >>> page.alternative
        m4 <= m3 <= m2 <= m1
        >>> page.L
        168.0
        >>> page.pvalue
        0.005454749182134622

    '''

    data = np.array(raw_data)

    # Number of blocks, number of products
    b, v = data.shape

    # Type checking
    if isinstance(rankings, str):

        if rankings == 'desc':
            y = np.arange(v, 0, -1)

        elif rankings == 'asc':
            y = np.arange(1, v + 1)

        else:
            raise ValueError(
                'String value of rankings must one of {"asc', 'desc"}')

    elif hasattr(rankings, "__len__"):
        y = np.array(rankings)

        # Check array sizes
        if len(y) != v:
            raise ValueError(
                'Data and hypothetical rankings have incompatible shapes.')

    # String for alternative hypothesis
    alt = ' <= '.join(['m%s' % i for i in y])

    # Design matrix (incidence matrix)
    blocks = np.logical_not(np.isnan(data)).astype('int')

    # Concurrence matrix
    #concurrence = np.dot(blocks.T, blocks)

    ki = blocks.sum(1)
    if (ki == ki[0]).all():
        k = ki[0]
    else:
        raise ValueError('Design is not balanced.')

    rj = blocks.sum(0)
    if (rj == rj[0]).all():
        r = rj[0]
    else:
        raise ValueError('Design is not balanced.')

    lambda_ = r*(k - 1)/(v - 1)

    # Check for missing data
    #if (np.isnan(data)).any():
    #    raise ValueError('Input array cannot have missing data.')

    # Rank the data
    ranked = np.apply_along_axis(rankdata, 1, data)

    # Column totals
    ranksum = np.nansum(ranked, axis=0)

    L = (y * ranksum).sum()

    if continuity_correction:
        contcorr = 0.5
    else:
        contcorr = 0.0

    #Z = (12*(L-contcorr) - 3*b*v*(v + 1)**2) /\
    #    (v*(v+1)*np.sqrt(b*(v - 1)))

    # PAGE'S STATISTIC IN BALANCED INCOMPLETE BLOCK DESIGNS
    # BENJAMIN S. DURAN, JR., B.S.
    # A THESIS IN STATISTICS
    Z = (12*(L-contcorr) - 3*b*k*(k + 1)*(v + 1)) /\
        (v * np.sqrt(lambda_*(v**2 - 1)*(k + 1)))

    # X = Z * Z # Chi-squared statistic
    # print(1 - chi2.cdf(X, 1))

    pvalue = 1 - norm.cdf(Z)

    return PageResult(alternative=alt, ranksum=ranksum, L=L, Z=Z,
                      pvalue=pvalue)


def main():

    data = [[3, 4, 2, 1],
            [4, 2, 1, 3],
            [4, 2, 3, 1],
            [4, 1, 3, 2],
            [2, 4, 3, 1],
            [4, 3, 1, 2]]

    page = page_test(data, rankings='desc', continuity_correction=False)
    print(page.ranksum)
    print(page.alternative)
    print(page.L)
    print(page.Z)
    print(page.pvalue)

if __name__ == '__main__':
    main()
