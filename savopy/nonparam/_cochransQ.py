# -*- coding: utf-8 -*-
# Friedman test for Two-way analysis of variance by ranks

import numpy as np
from scipy.stats import chi2
from collections import namedtuple

CochransQ = namedtuple('CochransQ', ['Q', 'pvalue', 'posthoc'])


def cochrans_q(raw_data):
    '''Cochran's Q test: Non-parametric testing of binary variables.

    Cochran's Q test is a non-parametric statistical test to verify whether
    k treatments have identical effects where the response variable can take
    only two possible outcomes.

    https://en.wikipedia.org/wiki/Cochran%27s_Q_test

    Args:
        raw_data (array-like): b * v array with data, where b is the number of
            judges and v is the number of product.

    Returns:
        CochransQ (namedtuple):
            Q (float): Chi-squared statistic of the data.
            pvalue (float): p-value of the chi-squared statistic.
            posthoc (np.array): v * v numpy array of p-values.
                If the p-value for the comparison of products i and j is below
                the significance level, then products i and j have
                significantly different ranks.

    Raises:
        ValueError: If the design has missing data.

    Example:

        >>> data = [[1, 1, 1],
        ...         [0, 1, 1],
        ...         [0, 0, 1],
        ...         [0, 1, 0],
        ...         [1, 0, 0],
        ...         [0, 1, 1],
        ...         [0, 1, 1],
        ...         [0, 0, 1],
        ...         [0, 1, 1],
        ...         [0, 1, 0],
        ...         [1, 1, 0],
        ...         [1, 1, 1],
        ...         [0, 0, 0],
        ...         [1, 0, 1],
        ...         [0, 1, 1],
        ...         [0, 1, 0],
        ...         [0, 0, 1],
        ...         [0, 1, 1],
        ...         [1, 0, 1],
        ...         [0, 1, 1]]
        >>> cochran = cochrans_q(data)
        >>> cochran.Q
        6.705882352941177
        >>> cochran.pvalue
        0.034981316424350095

    '''

    data = np.array(raw_data).astype(bool)

    # Number of blocks, number of products
    b, v = data.shape
    # r, k, lambda_ = b, v, b

    # Check for missing data
    if (np.isnan(data)).any():
        raise ValueError('Input array cannot have missing data.')

    colsum = data.sum(axis=0)
    rowsum = data.sum(axis=1)
    total = data.sum()
    Q = v*(v - 1) * ((colsum - total / v) ** 2).sum() /\
        (rowsum*(v - rowsum)).sum()

    p_value = 1 - chi2.cdf(Q, v - 1)

    # Post-hoc tests (Marascuillo)
    proportions = colsum / b
    diffs = proportions.reshape((v, 1)).T - proportions.reshape((v, 1))
    A = proportions * (1 - proportions) / b
    err2 = A.reshape((v, 1)).T + A.reshape((v, 1))

    #posthoc = 2 * (1 - t.cdf(abs(diffs) / err2, v - 1))
    posthoc = 1 - chi2.cdf(diffs ** 2 / err2, v - 1)

    return CochransQ(Q=Q, pvalue=p_value, posthoc=posthoc)


def main():

    data = [[1, 1, 1],
            [0, 1, 1],
            [0, 0, 1],
            [0, 1, 0],
            [1, 0, 0],
            [0, 1, 1],
            [0, 1, 1],
            [0, 0, 1],
            [0, 1, 1],
            [0, 1, 0],
            [1, 1, 0],
            [1, 1, 1],
            [0, 0, 0],
            [1, 0, 1],
            [0, 1, 1],
            [0, 1, 0],
            [0, 0, 1],
            [0, 1, 1],
            [1, 0, 1],
            [0, 1, 1]]

    cochran = cochrans_q(data)
    print(cochran.Q)
    print(cochran.pvalue)
    print(cochran.posthoc)

if __name__ == '__main__':
    main()
