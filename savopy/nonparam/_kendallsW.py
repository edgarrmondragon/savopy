# -*- coding: utf-8 -*-
# Kendall's coefficient of concordance (W).

import numpy as np
from scipy.stats import rankdata


def kendalls_w(raw_data, correct_ties=True):
    '''Kendall's W: Kendall's coefficient of concordance (W).

    It is a normalization of the statistic of the Friedman test, and can be
    used for assessing agreement among raters. Kendall's W ranges from 0
    (no agreement) to 1 (complete agreement).

    https://en.wikipedia.org/wiki/Kendall's_W

    Gibbons, Jean Dickinson; Chakraborti, Subhabrata (2003). Nonparametric
        Statistical Inference (4th ed.). New York: Marcel Dekker. p. 476-482.
        ISBN 0-8247-4052-1.

    Args:
        raw_data (array-like): b * v array with data, where b is the number of
            judges and v is the number of product. The experimental design is
            expected to be a Balanced Incomplete Block Design (BIBD).
        correct_ties (bool): whether the result should be corrected for ties in
            the rankings. It is currently available only for balanced designs.

    Returns:
        kendall (float): Kendall's coefficient of concordance (W).

    Raises:
        ValueError: If the design is not balanced, meaning products are not
            ranked the same number of times and/or judges don't rank the same
            number of products.

    Example:

        >>> data = [[7, np.nan, 5, 3, np.nan],
        ...         [np.nan, 8, np.nan, 4, 7],
        ...         [4, np.nan, 5, np.nan, 6],
        ...         [6, 7, np.nan, 5, np.nan],
        ...         [np.nan, 9, 7, np.nan, 6],
        ...         [4, 5, 6, np.nan, np.nan],
        ...         [np.nan, 9, 3, 5, np.nan],
        ...         [np.nan, np.nan, 5, 3, 4],
        ...         [5, np.nan, np.nan, 3, 7],
        ...         [7, 8, np.nan, np.nan, 2]]
        >>> kendalls_w(data)
        0.5777

    '''

    data = np.array(raw_data)

    # Number of blocks, number of products
    b, v = data.shape

    # Design matrix (incidence matrix)
    blocks = np.logical_not(np.isnan(data)).astype('int')

    # Concurrence matrix
    #concurrence = np.dot(blocks.T, blocks)

    ki = blocks.sum(1)
    if (ki == ki[0]).all():
        k = ki[0]
    else:
        raise ValueError('Design is not balanced.')

    rj = blocks.sum(0)
    if (rj == rj[0]).all():
        r = rj[0]
    else:
        raise ValueError('Design is not balanced.')

    lambda_ = r*(k - 1)/(v - 1)

    ranked = np.apply_along_axis(rankdata, 1, data)
    ranked[ranked > k] = np.nan

    # Ties correction
    def block_ties(block):
        counts = np.unique(block, return_counts=True)[1]
        return (counts * (counts ** 2 - 1)).sum()

    if r < b:
        print('WARNING: design is incomplete. No correction for ties will ' +
              'be applied')
        ties = 0
    elif correct_ties:
        ties = np.apply_along_axis(lambda row: block_ties(row),
                                   1, ranked).sum()
    else:
        ties = 0

    # Column totals
    ranksum = np.nansum(ranked, axis=0)

    # Kendall's Coefficient Of Concordance (W)
    S = ((ranksum - r*(k + 1)/2)**2).sum()
    kendall = 12*S / ((lambda_ ** 2)*v*(v**2 - 1) - ties)

    return kendall
