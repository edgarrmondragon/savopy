# -*- coding: utf-8 -*-
# Friedman test for Two-way analysis of variance by ranks

import numpy as np
from scipy.stats import rankdata, chi2, norm
from collections import namedtuple

FriedmanResult = namedtuple('FriedmanResult',
                            ['ranksum', 'Q', 'pvalue', 'posthoc'])


def friedman_test(raw_data, correct_ties=False):
    '''Friedmamn Test: Two-way analysis of variance by ranks.

    The Friedman test is a non-parametric statistical test developed by Milton
    Friedman. Similar to the parametric repeated measures ANOVA, it is used to
    detect differences in treatments across multiple test attempts. The
    procedure involves ranking each row (or block) together, then considering
    the values of ranks by columns. Applicable to complete block designs, it is
    thus a special case of the Durbin test.

    https://en.wikipedia.org/wiki/Friedman_test

    Args:
        raw_data (array-like): b * v array with data, where b is the number of
            judges and v is the number of product.
        correct_ties (bool): whether the result should be corrected for ties in
            the rankings. It is currently available only for balanced designs.

    Returns:
        FriedmanResult (namedtuple):
            ranksum (np.array): a one-dimensional numpy array with the sum of
                ranks for every product.
            Q (float): Chi-squared statistic of the data.
            pvalue (float): p-value of the chi-squared statistic.
            posthoc (np.array): v * v numpy array of p-values.
                If the p-value for the comparison of products i and j is below
                the significance level, then products i and j have
                significantly different ranks.

    Raises:
        ValueError: If the design has missing data.

    Example:

        >>> data = [[14, 23, 26, 30],
        ...         [19, 25, 25, 33],
        ...         [17, 22, 29, 28],
        ...         [17, 21, 28, 27],
        ...         [16, 24, 28, 32],
        ...         [15, 23, 27, 36],
        ...         [18, 26, 27, 26],
        ...         [16, 22, 30, 32]]
        >>> friedman = friedman_test(data)
        >>> friedman.Q
        20.576923076923077
        >>> friedman.pvalue
        0.00012887081500378983

    '''

    data = np.array(raw_data)

    # Number of blocks, number of products
    b, v = data.shape
    # r, k, lambda_ = b, v, b

    # Check for missing data
    if (np.isnan(data)).any():
        raise ValueError('Input array cannot have missing data.')

    # Rank the data
    ranked = np.apply_along_axis(rankdata, 1, data)

    # Column totals
    ranksum = np.nansum(ranked, axis=0)

    # Ties correction
    def block_ties(block):
        counts = np.unique(block, return_counts=True)[1]
        return (counts * (counts ** 2 - 1)).sum()

    if correct_ties:
        ties = np.apply_along_axis(lambda row: block_ties(row),
                                   1, ranked).sum()
    else:
        ties = 0

    S = ((ranksum - b*(v + 1) / 2) ** 2).sum()
    Q = 12*(v - 1)*S / (b*v*(v**2 - 1) - ties)

    # Chi-squared
    p_value = 1 - chi2.cdf(Q, v - 1)

    # Multiple comparisons (Dunn)
    err = np.sqrt(b*v*(v + 1) / 6)
    diffs = ranksum.reshape((v, 1)).T - ranksum.reshape((v, 1))

    posthoc = 2 * (1 - norm.cdf(abs(diffs) / err))

    return FriedmanResult(ranksum=ranksum, Q=Q, pvalue=p_value,
                          posthoc=posthoc)


def main():

    data = [[14, 23, 26, 30],
            [19, 25, 25, 33],
            [17, 22, 29, 28],
            [17, 21, 28, 27],
            [16, 24, 28, 32],
            [15, 23, 27, 36],
            [18, 26, 27, 26],
            [16, 22, 30, 32]]

    friedman = friedman_test(data, correct_ties=True)
    print(friedman.ranksum)
    print(friedman.Q)
    print(friedman.pvalue)
    print(friedman.posthoc)

if __name__ == '__main__':
    main()
