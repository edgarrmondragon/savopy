# -*- coding: utf-8 -*-
# Durbin test

import numpy as np
from scipy.stats import rankdata, chi2, f, t
from collections import namedtuple

DurbinResult = namedtuple('DurbinResult',
                          ['ranksum', 'T1', 'pvalue1', 'T2', 'pvalue2',
                           'posthoc'])


def durbin_test(raw_data):
    '''Durbin Test: Analysis of Balanced Incomplete Block Designs.

    In the analysis of designed experiments, the Friedman test is the most
    common non-parametric test for complete block designs. The Durbin test is a
    nonparametric test for balanced incomplete designs that reduces to the
    Friedman test in the case of a complete block design.

    https://en.wikipedia.org/wiki/Durbin_test

    Args:
        raw_data (array-like): b * v array with data, where b is the number of
            judges and v is the number of product. The experimental design is
            expected to be a Balanced Incomplete Block Design (BIBD).

    Returns:
        DurbinResult (namedtuple):
            ranksum (np.array): a one-dimensional numpy array with the sum of
                ranks for every product.
            T1 (float): Chi-squared statistic of the data.
            pvalue1 (float): p-value of the chi-squared statistic.
            T2 (float): F statistic of the data.
            pvalue2 (float): p-value of the F statistic.
            posthoc (np.array): v * v numpy array of p-values. If the p-value
                for the comparison of products i and j is below the
                significance level, then products i and j have significantly
                different ranks.

    Raises:
        ValueError: If the design is not balanced, meaning products are not
            ranked the same number of times and/or judges don't rank the same
            number of products.

    Example:

        >>> data = [[7, np.nan, 5, 3, np.nan],
        ...         [np.nan, 8, np.nan, 4, 7],
        ...         [4, np.nan, 5, np.nan, 6],
        ...         [6, 7, np.nan, 5, np.nan],
        ...         [np.nan, 9, 7, np.nan, 6],
        ...         [4, 5, 6, np.nan, np.nan],
        ...         [np.nan, 9, 3, 5, np.nan],
        ...         [np.nan, np.nan, 5, 3, 4],
        ...         [5, np.nan, np.nan, 3, 7],
        ...         [7, 8, np.nan, np.nan, 2]]
        >>> durbin = durbin_test(data)
        >>> durbin.T1
        10.4
        >>> durbin.pvalue1
        0.034202699408716786
        >>> durbin.T2
        4.3333333333333339

    '''

    data = np.array(raw_data)

    # Number of blocks, number of products
    b, v = data.shape

    # Design matrix (incidence matrix)
    blocks = np.logical_not(np.isnan(data)).astype('int')

    # Concurrence matrix
    #concurrence = np.dot(blocks.T, blocks)

    ki = blocks.sum(1)
    if (ki == ki[0]).all():
        k = ki[0]
    else:
        raise ValueError(
            'Design is not balanced, the Durbin Test is not appropriate.')

    rj = blocks.sum(0)
    if (rj == rj[0]).all():
        r = rj[0]
    else:
        raise ValueError(
            'Design is not balanced, the Durbin Test is not appropriate.')

    #lambda_ = r * (k - 1) / (v - 1)

    ranked = np.apply_along_axis(rankdata, 1, data)
    ranked[ranked > k] = np.nan

    # Column totals
    ranksum = np.nansum(ranked, axis=0)

    df1 = k - 1
    df2 = b * k - b - v + 1

    # Durbin's test statistics
    A = np.nansum(ranked ** 2)
    C = b * k * ((k + 1) ** 2) / 4
    T1 = ((v - 1) / (A - C)) * ((ranksum ** 2).sum() - r * C)
    T2 = (T1 / (v - 1)) / ((b * k - b - T1) / (b * k - b - v + 1))

    # Chi-squared
    p_value1 = 1 - chi2.cdf(T1, v - 1)

    # F statistic
    p_value2 = (1 - f.cdf(T2, df1, df2))

    # Multiple comparisons
    # Conover's procedure
    err = np.sqrt((2*(A-C)*r / (df2)) * (1-T1/(b*(k-1))))
    diffs = ranksum.reshape((v, 1)).T - ranksum.reshape((v, 1))

    posthoc = 2 * (1 - t.cdf(abs(diffs) / err, df2))

    return DurbinResult(ranksum=ranksum, T1=T1, pvalue1=p_value1,
                        T2=T2, pvalue2=p_value2, posthoc=posthoc)


def main():

    data = [[7, np.nan, 5, 3, np.nan],
            [np.nan, 8, np.nan, 4, 7],
            [4, np.nan, 5, np.nan, 6],
            [6, 7, np.nan, 5, np.nan],
            [np.nan, 9, 7, np.nan, 6],
            [4, 5, 6, np.nan, np.nan],
            [np.nan, 9, 3, 5, np.nan],
            [np.nan, np.nan, 5, 3, 4],
            [5, np.nan, np.nan, 3, 7],
            [7, 8, np.nan, np.nan, 2]]

    durbin = durbin_test(data)
    print(durbin.ranksum)
    print(durbin.T1)
    print(durbin.pvalue1)
    print(durbin.T2)
    print(durbin.pvalue2)
    print(durbin.posthoc)

if __name__ == '__main__':
    main()
