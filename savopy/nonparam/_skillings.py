# -*- coding: utf-8 -*-
# The Skillings–Mack test (Friedman test when there are missing data)
# https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2761045/

import numpy as np
from numpy.linalg import pinv
from scipy.stats import rankdata, chi2
from collections import namedtuple

SkillingsMack = namedtuple('SkillingsMack',
                           ['ranksum', 'SM', 'pvalue', 'corrected_sums'])


def skillings_mack(raw_data):
    '''Skillings-Mack Test: Friedman test when there are missing data.

    The Friedman test is a non-parametric statistical test developed by Milton
    Friedman. Similar to the parametric repeated measures ANOVA, it is used to
    detect differences in treatments across multiple test attempts. The
    procedure involves ranking each row (or block) together, then considering
    the values of ranks by columns. Applicable to complete block designs, it is
    thus a special case of the Durbin test.

    https://en.wikipedia.org/wiki/Friedman_test

    Args:
        raw_data (array-like): b * v array with data, where b is the number of
            judges and v is the number of product.

    Returns:
        FriedmanResult (namedtuple):
            ranksum (np.array): a one-dimensional numpy array with the sum of
                ranks for every product.
            SM (float): Chi-squared statistic for the test.
            pvalue (float): p-value of the chi-squared statistic.
            corrected_sums (array of floats): product rank sums corrected for
                missing data.

    Example:

        >>> data = [[3.2, 4.1, 3.8, 4.2],
        ...         [3.1, 3.9, 3.4, 4.0],
        ...         [4.3, 3.5, 4.6, 4.8],
        ...         [3.5, 3.6, 3.9, 4.0],
        ...         [3.6, 4.2, 3.7, 3.9],
        ...         [4.5, 4.7, 3.7, np.nan],
        ...         [np.nan, 4.2, 3.4, np.nan],
        ...         [np.nan, 4.2, np.nan, np.nan],
        ...         [4.3, 4.6, 4.4, 4.9],
        ...         [3.5, np.nan, 3.7, 3.9]]
        >>> skmack = skillings_mack(data)
        >>> skmack.SM
        15.493048797529271
        >>> skmack.pvalue
        0.0014402963024908733

    '''

    data = np.array(raw_data)

    # Number of blocks, number of products
    b, v = data.shape

    # Design matrix (incidence matrix)
    incidence = np.logical_not(np.isnan(data)).astype('int')
    ki = incidence.sum(1)

    # Remove any block with only one observation.
    data = data[ki > 1]

    # Recalculate
    incidence = np.logical_not(np.isnan(data)).astype('int')
    b, v = data.shape
    ki = incidence.sum(1)
    rj = incidence.sum(0)

    ranked = np.apply_along_axis(rankdata, 1, data)
    ranksum = np.nansum(ranked, axis=0)
    Aj = rj.astype(float)

    for i in range(b):
        ranked[i][ranked[i] > ki[i]] = (ki[i] + 1) / 2

    for j in range(v):
        Aj[j] = (np.sqrt(12 / (ki + 1)) * (ranked[:, j] - (ki + 1) / 2)).sum()

    concurrence = np.dot(incidence.T, incidence)
    notdiag = np.diag(rj) - concurrence
    S0 = np.diag(- notdiag.sum(1)) + notdiag
    sm = np.dot(np.dot(Aj, pinv(S0)), Aj.T)

    # Chi-squared
    p_value = 1 - chi2.cdf(sm, v - 1)

    return SkillingsMack(ranksum=ranksum, SM=sm, pvalue=p_value,
                         corrected_sums=Aj)


def main():

    data = [[3.2, 4.1, 3.8, 4.2],
            [3.1, 3.9, 3.4, 4.0],
            [4.3, 3.5, 4.6, 4.8],
            [3.5, 3.6, 3.9, 4.0],
            [3.6, 4.2, 3.7, 3.9],
            [4.5, 4.7, 3.7, np.nan],
            [np.nan, 4.2, 3.4, np.nan],
            [np.nan, 4.2, np.nan, np.nan],
            [4.3, 4.6, 4.4, 4.9],
            [3.5, np.nan, 3.7, 3.9]]

    skmack = skillings_mack(data)
    print(skmack.ranksum)
    print(skmack.SM)
    print(skmack.pvalue)
    print(skmack.corrected_sums)

if __name__ == '__main__':
    main()
