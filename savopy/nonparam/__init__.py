from ._durbin import durbin_test
from ._page import page_test
from ._friedman import friedman_test
from ._kendallsW import kendalls_w
from ._cochransQ import cochrans_q
from ._skillings import skillings_mack


__doc__ = 'Methods for non-parametric hypothesis testing: \n\n' + \
    durbin_test.__doc__.split('\n')[0] + '\n' + \
    page_test.__doc__.split('\n')[0] + '\n' + \
    friedman_test.__doc__.split('\n')[0] + '\n' + \
    kendalls_w.__doc__.split('\n')[0] + '\n' + \
    cochrans_q.__doc__.split('\n')[0] + '\n' + \
    skillings_mack.__doc__.split('\n')[0]
