# -*- coding: utf-8 -*-
# Correspondence Analysis of Cross Tables

import numpy as np
from numpy.linalg import svd
from collections import namedtuple


CorrespondenceAnalysis = namedtuple('CorrespondenceAnalysis',
                                    ['count_rows',
                                     'count_cols',
                                     'inertia',
                                     'singular_values',
                                     'explained_variance',
                                     'row_standard',
                                     'row_principal',
                                     'col_standard',
                                     'col_principal'
                                     ])


def correspondence(table):
    """Correspondence analysis provides tools for analyzing the associations
    between rows and columns of contingency tables. A contingency table is a
    two-entry frequency table where the joint frequencies of two qualitative
    variables are reported. For instance a (2 x 2) table could be formed by
    observing from a sample of n individuals two qualitative variables: the
    individual's sex and whether the individual smokes. The table reports the
    observed joint frequencies. In general (n x p) tables may be considered.

    Härdle, W. K., & Simar, L. (2015). Applied Multivariate Statistical
    Analysis. Berlin: Springer Berlin.

    Args:
        table (2d array): n x m array corresponding to a contingency table.

    Returns:
        CorrespondenceAnalysis (namedtuple):
            count_rows (float): number of rows in the table.
            count_cols (float): number of columns in the table.
            inertia (float): total inertia of the analysis.
            singular_values (1d float array): relative proportion of
                variability explained by each dimension.
            explained_variance (1d float array): percent of variability
                explained by each dimension
            row_standard (float): n x p table of standard coordinates of the
                rows.
            row_principal (float): n x p table of principal coordinates of the
                rows.
            col_standard (float): n x p table of standard coordinates of the
                columns.
            col_principal (float): n x p table of principal coordinates of the
                columns.

    Raises:
        ValueError: If the input table has missing data.
    """

    # Check for missing data
    if (np.isnan(np.array(table))).any():
        raise ValueError('Input array cannot have missing data.')

    # Convert data table to a Numpy matrix
    X = np.matrix(table)
    nrows, ncols = X.shape

    g = X.sum()  # Grand total
    S = X / g  # Reduced table
    wm = S.sum(axis=1)  # Reduced row totals
    wn = S.sum(axis=0)  # Reduced column totals
    E = wm * wn  # Expected values

    # "Departures" between observed and expected values, under independence
    M = (S - E) / np.sqrt(E)
    inertia = np.power(M, 2).sum()  # Total inertia

    # Singular Value Decomposition
    U, s, Vt = svd(M, full_matrices=False)
    singular_values = s ** 2

    explained_variance = singular_values / singular_values.sum()

    dr = np.diagflat(1 / np.sqrt(wm))
    dc = np.diagflat(1 / np.sqrt(wn))

    row_standard = dr * U
    row_principal = dr * U * np.diagflat(s)

    col_standard = dc * Vt.T
    col_principal = dc * Vt.T * np.diagflat(s)

    return CorrespondenceAnalysis(
        count_rows=nrows,
        count_cols=ncols,
        inertia=inertia,
        singular_values=singular_values,
        explained_variance=explained_variance,
        row_standard=row_standard,
        row_principal=row_principal,
        col_standard=col_standard,
        col_principal=col_principal
        )


def main():
    pass

if __name__ == '__main__':
    main()
