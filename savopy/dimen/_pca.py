# -*- coding: utf-8 -*-
# Correlation and Covariance Principal Components Analys

import numpy as np
from numpy.linalg import svd
from collections import namedtuple


CorrelationPCA = namedtuple(
    'CorrelationPCA',
    [
        'correlation_matrix',
        'eigenvalues',
        'explained_variance',
        'eigenvectors',
        'factor_loadings',
        'factor_scores'
    ])


def correlation_pca(raw_data, active_variables='all',
                    active_observations='all',
                    correlation=True):
    """TODO"""

    # Check for missing data
    if (np.isnan(np.array(raw_data))).any():
        raise ValueError('Input array cannot have missing data.')

    # Convert data table to a Numpy matrix
    data = np.matrix(raw_data)
    nrows, ncols = data.shape

    if active_variables == 'all':
        mask_var = np.array([True] * ncols)
    else:
        try:
            mask_var = np.array(active_variables).astype('bool')
        except:
            # TODO
            pass

    if active_observations == 'all':
        mask_obs = np.array([True] * nrows)
    else:
        try:
            mask_obs = np.array(active_observations).astype('bool')
        except:
            # TODO
            pass

    nvars = mask_var.sum()
    nobss = mask_obs.sum()

    X = data[:, mask_var][mask_obs, :]

    u, s, v = svd(X)

    print(nvars, nobss, s)


def main():
    pass

if __name__ == '__main__':
    main()
