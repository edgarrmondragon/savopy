from ._correspondence import correspondence


__doc__ = 'Methods for dimensionality reduction: \n\n' + \
    correspondence.__doc__.split('\n')[0]
