# savopy

A python package for the analysis of sensory and consumer data that sits upon numpy and scipy. It includes utilities for the analysis of sensory discrimination tests and for the analysis of incomplete block designs.

## Getting Started

### Prerequisites

<ul>
    <li>numpy >= 1.13.3</li>
    <li>scipy >= 0.19.1</li>
</ul>

### Installation

Clone the repository

```
$ git clone git@gitlab.com:edgarrmondragon/savopy.git
```

Install the package

```
$ python setup.py install
```

## Usage

```
>>> import numpy as np
>>> from savopy.nonparam import durbin_test
>>> data = [[7, np.nan, 5, 3, np.nan],
...         [np.nan, 8, np.nan, 4, 7],
...         [4, np.nan, 5, np.nan, 6],
...         [6, 7, np.nan, 5, np.nan],
...         [np.nan, 9, 7, np.nan, 6],
...         [4, 5, 6, np.nan, np.nan],
...         [np.nan, 9, 3, 5, np.nan],
...         [np.nan, np.nan, 5, 3, 4],
...         [5, np.nan, np.nan, 3, 7],
...         [7, 8, np.nan, np.nan, 2]]
>>> durbin = durbin_test(data)
>>> durbin.T1
10.4
>>> durbin.pvalue1
0.034202699408716786
>>> durbin.T2
4.3333333333333339
```

## Authors

* **Edgar Ramírez Mondragón** - [edgarrmondragon](https://gitlab.com/edgarrmondragon)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.
